/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscaRey;

import javax.swing.Icon;

/**
 *
 * @author alumno
 */
public class Naipe {
    public static final int ANCHO = 80;
    public static final int ALTO = 122;
    private final Icon ANVERSO;
    private static final Icon REVERSO = Baraja.REVERSO;
    private final float VALOR;
    private boolean repartido;
    private final int PALO;
    private final int NUMERO;
    boolean isRepartido;

    public Naipe(Icon ANVERSO, int p, int n, float VALOR) {
        this.ANVERSO = ANVERSO;
        this.repartido = false;
        this.PALO = p;
        this.NUMERO = n;
        this.VALOR = VALOR;
    }

    public Icon getANVERSO() {
        return ANVERSO;
    }

    public static Icon getREVERSO() {
        return REVERSO;
    }

    public float getVALOR() {
        return VALOR;
    }

    public int getPALO() {
        return PALO;
    }

    public int getNUMERO() {
        return NUMERO;
    }
    
    public boolean isRepartido(){
        return repartido;
    }

    public void setRepartido(boolean repartido) {
        this.repartido = repartido;
    }
    
}
