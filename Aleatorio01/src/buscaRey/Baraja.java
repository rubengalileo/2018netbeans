/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscaRey;

import java.util.ArrayList;
import java.util.Collections;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author alumno
 */
public class Baraja {
    private final ArrayList<Naipe> mazo;
    public static final Icon REVERSO = new ImageIcon("src/img/reverso.png");
    public static final Icon REY_OROS = new ImageIcon("src/img/12oros.png");
    public static final Icon CABALLO_OROS = new ImageIcon("src/img/11oros.png");
    public static final Icon SOTA_OROS = new ImageIcon("src/img/10oros.png");
    public static final int OROS = 0;
    public static final int COPAS = 1;
    public static final int ESPADAS = 2;
    public static final int BASTOS = 3;
            
    public Baraja(){
        mazo = new ArrayList<Naipe>();
        mazo.add(new Naipe(SOTA_OROS,OROS,10,0));
        mazo.add(new Naipe(CABALLO_OROS,OROS,11,0));
        mazo.add(new Naipe(REY_OROS,OROS,12,0));
        barajar();
    }
    public void barajar(){
        Collections.shuffle(mazo);
        for(Naipe n:mazo){
            n.setRepartido(false);
        }
    }
    public Naipe sacarNaipe(){
        for(Naipe n:mazo){
            if(n.isRepartido==false){
                n.setRepartido(false);
            }
        }
        
    }
    
}
