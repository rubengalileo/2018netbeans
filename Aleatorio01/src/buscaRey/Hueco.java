/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscaRey;

import javax.swing.JLabel;

/**
 *
 * @author alumno
 */
public class Hueco extends JLabel{
    private Naipe naipe;
    
    public Hueco(Naipe naipe){
        super(naipe.getREVERSO());
        this.naipe = naipe;
    }
    
    public void setNaipe(Naipe naipe){
        this.naipe = naipe;
        verREVERSO();
    }

    public Naipe getNaipe() {
        return naipe;
    }
    
    public void verREVERSO(){
        this.setIcon(naipe.getREVERSO());
    }
    
    public void verANVERSO(){
        this.setIcon(naipe.getANVERSO());
    }
}
